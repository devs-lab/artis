/**
 * @file artis/builder/ModelFactory.hpp
 * @author See the AUTHORS file
 */

/*
 * Copyright (C) 2012-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ARTIS_BUILDER_MODEL_FACTORY_HPP
#define __ARTIS_BUILDER_MODEL_FACTORY_HPP

#include <algorithm>
#include <memory>
#include <mutex>
#include <typeinfo>

#include <boost/core/demangle.hpp>

#include <artis/kernel/AbstractModel.hpp>

namespace artis {
    namespace builder {

        template<typename M, typename U, typename V>
        class ObjectCreator {
        public:
            ObjectCreator() = default;

            virtual ~ObjectCreator() = default;

            virtual std::string get() const = 0;

            virtual M* operator()(const artis::kernel::AbstractModels<U, V>& /* submodels */) const = 0;
        };

        template<typename M, typename I, typename O, typename U, typename V>
        class ModelFactory {
            typedef ModelFactory<M, I, O, U, V> type;

        public:
            virtual ~ModelFactory() = default;

            bool add(const I& id, O* creator)
            {
                std::lock_guard<std::mutex> lock(_mutex);
                return _creators.insert(typename Creators::value_type(
                        creator->get(),
                        std::make_pair(id, creator))).second;
            }

            M* create(const std::string& id, const artis::kernel::AbstractModels<U, V>& submodels)
            {
                std::lock_guard<std::mutex> lock(_mutex);
                typename Creators::const_iterator it = _creators.find(id);

                if (it != _creators.end()) {
                    return (*it->second.second)(submodels);
                } else {
                    return nullptr;
                }
            }

            static ModelFactory& factory()
            {
                std::call_once(_flag, []() { _instance.reset(new ModelFactory()); });
                return *_instance;
            }

            int make_id()
            {
                std::lock_guard<std::mutex> lock(_mutex);
                return ++_id;
            }

            bool remove(const I& id)
            {
                std::lock_guard<std::mutex> lock(_mutex);
                typename Creators::const_iterator it = _creators.find(id);

                if (it != _creators.end()) {
                    _creators.erase(it);
                    return true;
                } else {
                    return false;
                }
            }

        private:
            ModelFactory() { _id = -1; }

            typedef std::pair<I, O*> Creator;
            typedef std::map<std::string, Creator> Creators;

            static std::shared_ptr<type> _instance;
            static std::once_flag _flag;
            std::mutex _mutex;

            Creators _creators;
            int _id;
        };

    }
}

template<typename M, typename I, typename O, typename U, typename V>
std::shared_ptr<artis::builder::ModelFactory<M, I, O, U, V> >
        artis::builder::ModelFactory<M, I, O, U, V>::_instance;

template<typename M, typename I, typename O, typename U, typename V>
std::once_flag artis::builder::ModelFactory<M, I, O, U, V>::_flag;

#define DECLARE_MODEL(mdl, type, fct, U, V)                            \
    class creator_##mdl : public artis::builder::ObjectCreator < type, U, V > { \
    public:                                                             \
    creator_##mdl()                                                     \
        {                                                               \
            fct::factory().add(fct::factory().make_id(), this);         \
        }                                                               \
    std::string get() const                                             \
        { return boost::core::demangle(typeid(mdl).name()); }           \
    type* operator()(const artis::kernel::AbstractModels < U, V >& submodels) const \
        { return new mdl(submodels); }                                  \
    static creator_##mdl an_##mdl;                                      \
    };                                                                  \
    creator_##mdl an_##mdl;

#endif
