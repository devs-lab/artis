/**
 * @file artis/utils/ParametersReader.hpp
 * See the AUTHORS file
 */

/*
 * Copyright (C) 2012-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_UTILS_PARAMETERS_READER_HPP
#define ARTIS_UTILS_PARAMETERS_READER_HPP

#include <string>

namespace artis {
    namespace utils {

        template<typename ModelParameters>
        class ParametersReader {
        public:
            ParametersReader() { }

            virtual ~ParametersReader() { }

            virtual void load(const std::string& id, ModelParameters& parameters) = 0;
        };

    }
}

#endif
