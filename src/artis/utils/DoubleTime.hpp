/**
 * @file artis/utils/DoubleTime.hpp
 * @author See the AUTHORS file
 */

/*
 * Copyright (C) 2012-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UTILS_DOUBLE_TIME
#define UTILS_DOUBLE_TIME

#include <artis/utils/Time.hpp>

#include <limits>

namespace artis {
    namespace utils {

        template<typename T>
        struct Limits {
            static constexpr T negative_infinity =
                    -std::numeric_limits<T>::infinity();
            static constexpr T positive_infinity =
                    std::numeric_limits<T>::infinity();
            static constexpr T null = 0;
        };

        typedef Time<double, Limits<double> > DoubleTime;

    }
}

#endif
