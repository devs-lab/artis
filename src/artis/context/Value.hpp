/**
 * @file artis/context/Value.hpp
 * @author See the AUTHORS file
 */

/*
 * Copyright (C) 2012-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ARTIS_CONTEXT_VALUE_HPP
#define __ARTIS_CONTEXT_VALUE_HPP

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/array.hpp>
#include <boost/version.hpp>

#if BOOST_VERSION > 106200

#include <boost/serialization/array_wrapper.hpp>

#endif

#include <algorithm>
#include <cassert>
#include <cstring>

#if WIN32
#include <string>
#endif

#include <typeinfo>
#include <vector>

namespace artis {
    namespace context {

        class Value {
        public:
            Value()
                    :_content(nullptr), _size(0) { }

            template<typename T>
            Value(T value)
                    : _content(nullptr), _size(0) { assign(&value, sizeof(T), typeid(T).hash_code()); }

            Value(void* content, size_t size)
                    :_content(nullptr), _size(0) { assign(content, size, typeid(void*).hash_code()); }

            Value(const char* value, unsigned int size)
                    :_content(nullptr), _size(0) { assign(value, size * sizeof(char), typeid(char*).hash_code()); }

            Value(const Value& value)
                    :_content(nullptr), _size(0)
            {
                if (value._content) {
                    assign(value._content, value._size, value._type_id);
                }
            }

            virtual ~Value() { if (_content) delete[] _content; }

            bool empty() const { return _content == nullptr; }

            template<typename T>
            void get_content(T& value) const
            {
                assert(_type_id == typeid(T).hash_code());

                value = *(T*) (_content);
            }

            const Value& operator=(const Value& value)
            {
                if (_content) delete[] _content;
                _content = nullptr;
                _size = 0;
                if (value._content) {
                    assign(value._content, value._size, value._type_id);
                }
                return *this;
            }

            std::string to_string() const
            {
                if (is_type<double>()) {
                    double v;

                    get_content(v);
                    return std::to_string(v);
                } else if (is_type<int>()) {
                    int v;

                    get_content(v);
                    return std::to_string(v);
                } else if (is_type<bool>()) {
                    bool v;

                    get_content(v);
                    return v ? "true" : "false";
                }
                if (is_type<std::vector<double> >()) {
                    std::vector<double> v;

                    get_content(v);
                    return "";
                } else if (is_type<std::vector<int> >()) {
                    std::vector<int> v;

                    get_content(v);
                    return "";
                } else if (is_type<std::vector<bool> >()) {
                    std::vector<bool> v;

                    get_content(v);
                    return "";
                } else {
                    assert(false);
                }
            }

            template<typename Z>
            bool is_type() const { return _type_id == typeid(Z).hash_code(); }

        private:
            void assign(const void* content, size_t size, size_t type_id)
            {
                _content = new char[size < 16 ? 16 : size];
                std::memcpy(_content, content, size);
                _size = size;
                _type_id = type_id;

            }

            friend class boost::serialization::access;

            template<class Archive>
            void serialize(Archive& ar, const unsigned int version)
            {
                (void) version;

                ar & _size;
                if (Archive::is_loading::value) {
                    assert(_content == nullptr);
                    _content = new char[_size];
                }
                ar & boost::serialization::make_array<char>(_content, _size);
                ar & _type_id;
            }

            char* _content;
            size_t _size;
            size_t _type_id;
        };

    }
}

#endif
