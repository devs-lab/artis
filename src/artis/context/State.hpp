/**
 * @file artis/context/State.hpp
 * @author See the AUTHORS file
 */

/*
 * Copyright (C) 2012-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ARTIS_KERNEL_STATE_HPP
#define __ARTIS_KERNEL_STATE_HPP

#include <artis/context/StateValues.hpp>

#include <map>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/map.hpp>
#include <boost/serialization/vector.hpp>

namespace artis {
    namespace context {

        template<typename U>
        class State {
            typedef std::map<unsigned int, State<U> > Substates;

        public:
            State()
                    :_last_time(-1) { }

            virtual ~State() { }

            void add_external(unsigned int variable_key, const Value& value)
            {
                _values.add_external(variable_key, value);
            }

            void add_internal(unsigned int variable_key, const Value& value)
            {
                _values.add_internal(variable_key, value);
            }

            void add_state(unsigned int variable_key, const Value& value) { _values.add_state(variable_key, value); }

            void add_substate(unsigned int model_key, State& state)
            {
                _substates.insert(std::make_pair(model_key, state));
            }

            const Value& get_external(unsigned int variable_key) const { return _values.get_external(variable_key); }

            const Value& get_internal(unsigned int variable_key) const { return _values.get_internal(variable_key); }

            const Value& get_state(unsigned int variable_key) const { return _values.get_state(variable_key); }

            const State<U>& get_substate(unsigned int model_key) const { return _substates.find(model_key)->second; }

            typename U::type last_time() const { return _last_time; }

            void last_time(typename U::type t) { _last_time = t; }

            std::string to_string() const
            {
                std::string str = "last_time: " + std::to_string(_last_time) +
                        "; values: " + _values.to_string() + "; sub_states: [ ";

                for (typename Substates::const_iterator it = _substates.begin();
                     it != _substates.end(); ++it) {
                    str += it->second.to_string() + " ";
                }
                str += "]";
                return str;
            }

        private:
            friend class boost::serialization::access;

            template<class Archive>
            void serialize(Archive& ar, const unsigned int version)
            {
                (void) version;

                ar & _values;
                ar & _substates;
                ar & _last_time;
            }

            StateValues _values;
            Substates _substates;
            typename U::type _last_time;
        };

    }
}

#endif
