/**
 * @file artis/context/Context.hpp
 * @author See the AUTHORS file
 */

/*
 * Copyright (C) 2012-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ARTIS_KERNEL_CONTEXT_HPP
#define __ARTIS_KERNEL_CONTEXT_HPP

#include <artis/context/State.hpp>

#include <boost/serialization/serialization.hpp>

namespace artis {
    namespace context {

        template<typename U>
        class Context {
        public:
            Context()
                    :_begin(-1), _end(-1), _valid(false) { }

            Context(double begin, double end)
                    :_begin(begin), _end(end), _valid(false) { }

            virtual ~Context() { }

            double begin() const { return _begin; }

            void begin(double begin) { _begin = begin; }

            double end() const { return _end; }

            void end(double end) { _end = end; }

            const Context& operator=(const Context& context)
            {
                _begin = context._begin;
                _end = context._end;
                _valid = context._valid;
                _state = context._state;
                return *this;
            }

            void saved() { _valid = true; }

            const State<U>& state() const { return _state; }

            State<U>& state() { return _state; }

            std::string to_string() const
            {
                return "begin: " + std::to_string(_begin) +
                        "; end: " + std::to_string(_end) +
                        "; valid: " + (_valid ? "true" : "false") +
                        "; state: " + _state.to_string();
            }

            bool valid() const { return _valid; }

        private:
            friend class boost::serialization::access;

            template<class Archive>
            void serialize(Archive& ar, const unsigned int version)
            {
                (void) version;

                ar & _begin;
                ar & _end;
                ar & _state;
                ar & _valid;
            }

            double _begin;
            double _end;
            State<U> _state;
            bool _valid;
        };

    }
}

#endif
