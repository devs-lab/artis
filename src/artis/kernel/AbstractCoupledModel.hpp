/**
 * @file artis/kernel/AbstractCoupledModel.hpp
 * @author See the AUTHORS file
 */

/*
 * Copyright (C) 2012-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ARTIS_KERNEL_ABSTRACT_COUPLED_MODEL_HPP
#define __ARTIS_KERNEL_ABSTRACT_COUPLED_MODEL_HPP

#include <artis/kernel/AbstractModel.hpp>
#include <artis/kernel/Externals.hpp>
#include <artis/kernel/Internals.hpp>
#include <artis/kernel/States.hpp>

#include <artis/utils/DateTime.hpp>
#include <artis/utils/Trace.hpp>

#include <functional>
#include <vector>

namespace artis {
    namespace kernel {

        template<typename T, typename U, typename V, typename W>
        class Simulator;

        template<typename T, typename U, typename V, typename W>
        class AbstractCoupledModel : public AbstractModel<U, V>,
                                     public States<T, U, V>,
                                     public Internals<T, U, V>,
                                     public Externals<T, U, V> {
            typedef AbstractModel<U, V> type;
            typedef std::map<int, type*> Submodels;
            typedef std::map<int,
                    std::vector<type*> > Setsubmodels;
            typedef std::map<int, std::pair<type*,
                    int> > SubModelInternals;

            using Externals<T, U, V>::restore;
            using Internals<T, U, V>::restore;
            using States<T, U, V>::restore;
            using Externals<T, U, V>::save;
            using Internals<T, U, V>::save;
            using States<T, U, V>::save;

#ifdef WITH_TRACE

            friend class Simulator<T, U, V, W>;

#endif

        public:
            struct Var {
                unsigned int index;
                type* model;
                int sub_index;
                std::string var_name;
                std::string sub_var_name;

                Var(unsigned int index, std::string var_name,
                        type* model, int sub_index, std::string sub_var_name)
                        :
                        index(index), model(model), sub_index(sub_index),
                        var_name(var_name), sub_var_name(sub_var_name) { }
            };

            AbstractCoupledModel(const type* parent = 0)
                    :type(parent)
            {
#ifdef WITH_TRACE
                utils::Trace<utils::DoubleTime>::trace()
                        << utils::TraceElement<utils::DoubleTime>(
                                true,
                                boost::core::demangle(typeid(T).name()),
                                utils::DoubleTime::null,
                                utils::CONSTRUCT);
                utils::Trace<utils::DoubleTime>::trace().flush();
#endif
            }

            virtual ~AbstractCoupledModel()
            {
#ifdef WITH_TRACE
                trace_element(true, utils::DoubleTime::null, utils::DESTRUCT);
#endif
            }

            virtual void after(typename U::type t)
            {

#ifdef WITH_TRACE
                trace_element(true, t, utils::AFTER_COMPUTE);
                trace_internals(true, t, utils::AFTER_COMPUTE);
#else
                (void) t;
#endif

            }

            virtual const Node<U>* atomic(unsigned int index) const
            {
                typename AbstractCoupledModel::SubModelInternals::const_iterator it =
                        submodel_internals.find(index);

                if (it == submodel_internals.end()) {
                    return this;
                } else {
                    return it->second.first->atomic(it->second.second);
                }
            }

            virtual void before(typename U::type t)
            {

#ifdef WITH_TRACE
                trace_element(true, t, utils::BEFORE_COMPUTE);
                trace_internals(true, t, utils::BEFORE_COMPUTE);
                trace_externals(true, t, utils::BEFORE_COMPUTE);
#else
                (void) t;
#endif

            }

            virtual void build(W& /* parameters */) { }

            virtual void build(W& /* parameters */, const std::string& /* json */) { }

            virtual bool check(typename U::type t) const { return Externals<T, U, V>::check(t); }

            virtual void compute(typename U::type t, bool update) = 0;

            virtual const Any& get(typename U::type t, unsigned int index) const
            {
                typename AbstractCoupledModel::SubModelInternals::const_iterator it =
                        submodel_internals.find(index);

                if (it == submodel_internals.end()) {
                    if (type::last_time != t) {
                        throw utils::InvalidGet("Variable not computed");
                    }
                    return Internals<T, U, V>::get(index);
                } else {
                    return it->second.first->get(t, it->second.second);
                }
            }

            template<typename Z, typename K>
            Z get(typename U::type t, unsigned int index) const
            {
                typename AbstractCoupledModel::SubModelInternals::const_iterator it =
                        submodel_internals.find(index);

                if (it == submodel_internals.end()) {
                    if (type::last_time != t) {
                        throw utils::InvalidGet("Variable not computed");
                    }
                    const Any& value = Internals<T, U, V>::get(index);

                    return static_cast < const T* >(this)->*(value.get<T, Z>());
                } else {
                    const Any& value = it->second.first->get(t, it->second.second);
                    const Node<U>* object = it->second.first->atomic(
                            it->second.second);

                    return static_cast < const K* >(object)->*(value.get<K, Z>());
                }
            }

            virtual std::string get(const ValueTypeID& value_type, typename U::type t,
                    unsigned int index) const
            {
                typename AbstractCoupledModel::SubModelInternals::const_iterator it =
                        submodel_internals.find(index);

                if (it == submodel_internals.end()) {
                    Any value = Internals<T, U, V>::get(index);

                    switch (value_type) {
                    case DOUBLE:
                        return std::to_string(
                                static_cast < const T* >(this)->*(value.get<T,
                                        double>()));
                    case INT:
                        return std::to_string(
                                static_cast < const T* >(this)->*(value.get<T, int>()));
                    case BOOL:
                        return std::to_string(
                                static_cast < const T* >(this)->*(value.get<T, bool>()));
                    default:
                        return "NA";
                    }
                } else {
                    return it->second.first->get(value_type, t, it->second.second);
                }
            }

            virtual void init(typename U::type t, const V& parameters) = 0;

            virtual bool is_atomic() const { return false; }

            bool is_computed(typename U::type t, unsigned int index) const
            {
                typename AbstractCoupledModel::SubModelInternals::const_iterator it =
                        submodel_internals.find(index);

                if (it == submodel_internals.end()) {
                    return false;
                } else {
                    return it->second.first->is_computed(t, it->second.second);
                }
            }

            bool is_stable(typename U::type t) const
            {
                typename AbstractCoupledModel::SubModelInternals::const_iterator it =
                        submodel_internals.begin();
                bool stable = true;

                while (it != submodel_internals.end() and stable) {
                    stable = it->second.first->is_stable(t);
                    ++it;
                }
                return stable;
            }

            virtual bool is_updated() const { return Externals<T, U, V>::updated; }

            virtual const Node<U>* get_submodel(unsigned int index) const
            {
                typename AbstractCoupledModel::Submodels::const_iterator it =
                        submodels.find(index);

                if (it != submodels.end()) {
                    return it->second;
                } else {
                    return 0;
                }
            }

            virtual const Node<U>* get_submodel(unsigned int index,
                    unsigned int rank) const
            {
                typename AbstractCoupledModel::Setsubmodels::const_iterator it =
                        setsubmodels.find(index);

                if (it != setsubmodels.end() and it->second.size() > rank) {
                    return it->second.at(rank);
                } else {
                    return 0;
                }
            }

            virtual std::string path_index(const AbstractModel<U, V>* child,
                    int& index) const
            {
                typename Setsubmodels::const_iterator it = setsubmodels.begin();
                bool found = false;

                index = -1;
                while (not found and it != setsubmodels.end()) {
                    typename std::vector<type*>::const_iterator itm =
                            it->second.begin();

                    index = 0;
                    while (not found and itm != it->second.end()) {
                        found = *itm == child;
                        if (not found) {
                            ++index;
                            ++itm;
                        }
                    }
                    index = found ? index : -1;
                    ++it;
                }
                if (type::parent) {
                    int i;
                    std::string p = type::parent->path_index(this, i);

                    if (i >= 0) {
                        return p +
                                "/[" + std::to_string(i) + "]" +
                                boost::core::demangle(typeid(*this).name());
                    } else {
                        return p + "/" + boost::core::demangle(typeid(*this).name());
                    }
                } else {
                    return boost::core::demangle(typeid(*this).name());
                }
            }

            virtual void restore(const context::State<U>& state)
            {
                typename AbstractCoupledModel::Submodels::const_iterator it =
                        submodels.begin();

                while (it != submodels.end()) {
                    it->second->restore(state.get_substate(it->first));
                    ++it;
                }
                States<T, U, V>::restore(this, state);
                Internals<T, U, V>::restore(this, state);
                Externals<T, U, V>::restore(this, state);
            }

            virtual void save(context::State<U>& state) const
            {
                typename AbstractCoupledModel::Submodels::const_iterator it =
                        submodels.begin();

                while (it != submodels.end()) {
                    context::State<U> substate;

                    it->second->save(substate);
                    state.add_substate(it->first, substate);
                    ++it;
                }
                States<T, U, V>::save(this, state);
                Internals<T, U, V>::save(this, state);
                Externals<T, U, V>::save(this, state);
            }

            virtual void stable() { Externals<T, U, V>::updated = false; }

            void submodel(unsigned int index, type* model)
            {
                if (model) {
                    submodels[index] = model;
                    model->set_parent(this);
                } else {
                    submodels[index] = 0;
                }
            }

            virtual void trace_element(typename U::type t,
                    utils::TraceType type = utils::NONE,
                    std::string comment = "") const { trace_element(false, t, type, comment); }

            virtual void trace_internals(typename U::type t,
                    utils::TraceType type) const { trace_internals(false, t, type); }

            virtual void trace_externals(typename U::type t,
                    utils::TraceType type = utils::NONE) const { trace_externals(false, t, type); }

            virtual void trace_model(typename U::type t,
                    utils::TraceType type = utils::NONE) const { trace_model(false, t, type); }

        private:
            void trace_element(bool from_kernel, typename U::type t,
                    utils::TraceType type = utils::NONE,
                    std::string comment = "") const
            {
                utils::Trace<utils::DoubleTime>::trace()
                        << utils::TraceElement<utils::DoubleTime>(
                                from_kernel,
                                AbstractCoupledModel<T, U, V, W>::path(this),
                                t, type)
                        << comment;
                utils::Trace<utils::DoubleTime>::trace().flush();
            }

            void trace_internals(bool from_kernel, typename U::type t,
                    utils::TraceType type) const
            {
                for (size_t i = 0; i < Internals<T, U, V>::size(); ++i) {
                    if (not Internals<T, U, V>::get(i).is_null()) {
                        utils::Trace<utils::DoubleTime>::trace()
                                << utils::TraceElement<utils::DoubleTime>(
                                        from_kernel,
                                        AbstractCoupledModel<T, U, V, W>::path(this),
                                        t, type)
                                << utils::KernelInfo(
                                        Internals<T, U, V>::name(i), true,
                                        Internals<T, U, V>::get(i).to_string(
                                                dynamic_cast < const T* >(this))
                                );
                        utils::Trace<utils::DoubleTime>::trace().flush();
                    }
                }
            }

            void trace_externals(bool from_kernel, typename U::type t,
                    utils::TraceType type = utils::NONE) const
            {
                for (size_t i = 0; i < Externals<T, U, V>::size(); ++i) {
                    if (not Externals<T, U, V>::get(i).is_null()) {
                        utils::Trace<utils::DoubleTime>::trace()
                                << utils::TraceElement<utils::DoubleTime>(
                                        from_kernel,
                                        AbstractCoupledModel<T, U, V, W>::path(this),
                                        t, type)
                                << utils::KernelInfo(
                                        Externals<T, U, V>::name(i), false,
                                        Externals<T, U, V>::get(i).to_string(
                                                dynamic_cast < const T* >(this))
                                );
                        utils::Trace<utils::DoubleTime>::trace().flush();
                    }
                }
            }

            void trace_model(bool from_kernel, typename U::type t,
                    utils::TraceType type = utils::NONE) const
            {
                trace_element(from_kernel, t, type);
                trace_internals(from_kernel, t, type);
                trace_externals(from_kernel, t, type);

                // typename AbstractCoupledModel::Submodels::const_iterator it =
                //     submodels.begin();

                // while (it != submodels.end()) {
                //     it->second->trace_model(from_kernel, t, type);
                //     ++it;
                // }
            }

        protected:
            void link_internal_(unsigned int index, std::string var_name,
                    type* model, int sub_index, std::string sub_var_name)
            {
#ifndef WITH_TRACE
                (void) var_name;
                (void) sub_var_name;
#endif

                submodel_internals[index] =
                        std::pair<type*, int>(model, sub_index);
#ifdef WITH_TRACE
                utils::Trace<utils::DoubleTime>::trace()
                        << utils::TraceElement<utils::DoubleTime>(
                                true,
                                AbstractCoupledModel<T, U, V, W>::path(this),
                                utils::DoubleTime::null,
                                utils::INTERNAL_LINK)
                        << utils::KernelInfo(
                                var_name,
                                AbstractCoupledModel<T, U, V, W>::path(model),
                                sub_var_name);
                utils::Trace<utils::DoubleTime>::trace().flush();

#endif
            }

            void I_(std::initializer_list<Var> internals)
            {
                for (typename std::initializer_list<Var>::iterator it =
                        internals.begin(); it != internals.end(); ++it) {

                    submodel_internals[it->index] =
                            std::pair<type*, int>(it->model, it->sub_index);

#ifdef WITH_TRACE
                    utils::Trace<utils::DoubleTime>::trace()
                            << utils::TraceElement<utils::DoubleTime>(
                                    true,
                                    AbstractCoupledModel<T, U, V, W>::path(this),
                                    utils::DoubleTime::null,
                                    utils::INTERNAL_LINK)
                            << utils::KernelInfo(
                                    it->var_name,
                                    AbstractCoupledModel<T, U, V, W>::path(it->model),
                                    it->sub_var_name);
                    utils::Trace<utils::DoubleTime>::trace().flush();
#endif
                }
            }

            void SM_(std::initializer_list<std::pair<unsigned int,
                    type*> > models)
            {
                for (typename std::initializer_list<std::pair<unsigned int,
                        type*> >::iterator it =
                        models.begin(); it != models.end(); ++it) {
                    submodels[it->first] = it->second;
                    it->second->set_parent(this);

#ifdef WITH_TRACE
                    utils::Trace<utils::DoubleTime>::trace()
                            << utils::TraceElement<utils::DoubleTime>(
                                    true,
                                    AbstractCoupledModel<T, U, V, W>::path(this),
                                    utils::DoubleTime::null,
                                    utils::SUBMODEL_ADD)
                            << utils::KernelInfo(
                                    AbstractCoupledModel<T, U, V, W>::path(it->second));
                    utils::Trace<utils::DoubleTime>::trace().flush();
#endif

                }
            }

            void setsubmodel(unsigned int index, type* model)
            {
                if (setsubmodels.find(index) == setsubmodels.end()) {
                    setsubmodels[index] = std::vector<type*>();
                }
                setsubmodels[index].push_back(model);
                model->set_parent(this);

#ifdef WITH_TRACE
                utils::Trace<utils::DoubleTime>::trace()
                        << utils::TraceElement<utils::DoubleTime>(
                                true,
                                AbstractCoupledModel<T, U, V, W>::path(this),
                                utils::DoubleTime::null,
                                utils::SUBMODEL_ADD)
                        << utils::KernelInfo(
                                AbstractCoupledModel<T, U, V, W>::path(model));
                utils::Trace<utils::DoubleTime>::trace().flush();
#endif
            }

        private:
            SubModelInternals submodel_internals;
            Submodels submodels;
            Setsubmodels setsubmodels;
        };

#define InternalS(index, var, sub_index) link_internal_(index, std::string(ESCAPEQUOTE(index)), var, sub_index, std::string(ESCAPEQUOTE(index)))
#define ITEM_S(index, var, sub_index) Var(index, std::string(ESCAPEQUOTE(index)), var, sub_index, std::string(ESCAPEQUOTE(index)))
#define UNWRAP_ITEM_S(...) ITEM_S __VA_ARGS__
#define LIST_S_16(L1, L2, L3, L4, L5, L6, L7, L8, L9, L10, L11, L12, L13, L14, L15, L16) { UNWRAP_ITEM_S(L1), UNWRAP_ITEM_S(L2), UNWRAP_ITEM_S(L3), UNWRAP_ITEM_S(L4), UNWRAP_ITEM_S(L5), UNWRAP_ITEM_S(L6), UNWRAP_ITEM_S(L7), UNWRAP_ITEM_S(L8), UNWRAP_ITEM_S(L9), UNWRAP_ITEM_S(L10), UNWRAP_ITEM_S(L11), UNWRAP_ITEM_S(L12), UNWRAP_ITEM_S(L13), UNWRAP_ITEM_S(L14), UNWRAP_ITEM_S(L15), UNWRAP_ITEM_S(L16) }
#define LIST_S_15(L1, L2, L3, L4, L5, L6, L7, L8, L9, L10, L11, L12, L13, L14, L15) { UNWRAP_ITEM_S(L1), UNWRAP_ITEM_S(L2), UNWRAP_ITEM_S(L3), UNWRAP_ITEM_S(L4), UNWRAP_ITEM_S(L5), UNWRAP_ITEM_S(L6), UNWRAP_ITEM_S(L7), UNWRAP_ITEM_S(L8), UNWRAP_ITEM_S(L9), UNWRAP_ITEM_S(L10), UNWRAP_ITEM_S(L11), UNWRAP_ITEM_S(L12), UNWRAP_ITEM_S(L13), UNWRAP_ITEM_S(L14), UNWRAP_ITEM_S(L15) }
#define LIST_S_14(L1, L2, L3, L4, L5, L6, L7, L8, L9, L10, L11, L12, L13, L14) { UNWRAP_ITEM_S(L1), UNWRAP_ITEM_S(L2), UNWRAP_ITEM_S(L3), UNWRAP_ITEM_S(L4), UNWRAP_ITEM_S(L5), UNWRAP_ITEM_S(L6), UNWRAP_ITEM_S(L7), UNWRAP_ITEM_S(L8), UNWRAP_ITEM_S(L9), UNWRAP_ITEM_S(L10), UNWRAP_ITEM_S(L11), UNWRAP_ITEM_S(L12), UNWRAP_ITEM_S(L13), UNWRAP_ITEM_S(L14) }
#define LIST_S_13(L1, L2, L3, L4, L5, L6, L7, L8, L9, L10, L11, L12, L13) { UNWRAP_ITEM_S(L1), UNWRAP_ITEM_S(L2), UNWRAP_ITEM_S(L3), UNWRAP_ITEM_S(L4), UNWRAP_ITEM_S(L5), UNWRAP_ITEM_S(L6), UNWRAP_ITEM_S(L7), UNWRAP_ITEM_S(L8), UNWRAP_ITEM_S(L9), UNWRAP_ITEM_S(L10), UNWRAP_ITEM_S(L11), UNWRAP_ITEM_S(L12), UNWRAP_ITEM_S(L13) }
#define LIST_S_12(L1, L2, L3, L4, L5, L6, L7, L8, L9, L10, L11, L12) { UNWRAP_ITEM_S(L1), UNWRAP_ITEM_S(L2), UNWRAP_ITEM_S(L3), UNWRAP_ITEM_S(L4), UNWRAP_ITEM_S(L5), UNWRAP_ITEM_S(L6), UNWRAP_ITEM_S(L7), UNWRAP_ITEM_S(L8), UNWRAP_ITEM_S(L9), UNWRAP_ITEM_S(L10), UNWRAP_ITEM_S(L11), UNWRAP_ITEM_S(L12) }
#define LIST_S_11(L1, L2, L3, L4, L5, L6, L7, L8, L9, L10, L11) { UNWRAP_ITEM_S(L1), UNWRAP_ITEM_S(L2), UNWRAP_ITEM_S(L3), UNWRAP_ITEM_S(L4), UNWRAP_ITEM_S(L5), UNWRAP_ITEM_S(L6), UNWRAP_ITEM_S(L7), UNWRAP_ITEM_S(L8), UNWRAP_ITEM_S(L9), UNWRAP_ITEM_S(L10), UNWRAP_ITEM_S(L11) }
#define LIST_S_10(L1, L2, L3, L4, L5, L6, L7, L8, L9, L10) { UNWRAP_ITEM_S(L1), UNWRAP_ITEM_S(L2), UNWRAP_ITEM_S(L3), UNWRAP_ITEM_S(L4), UNWRAP_ITEM_S(L5), UNWRAP_ITEM_S(L6), UNWRAP_ITEM_S(L7), UNWRAP_ITEM_S(L8), UNWRAP_ITEM_S(L9), UNWRAP_ITEM_S(L10) }
#define LIST_S_9(L1, L2, L3, L4, L5, L6, L7, L8, L9) { UNWRAP_ITEM_S(L1), UNWRAP_ITEM_S(L2), UNWRAP_ITEM_S(L3), UNWRAP_ITEM_S(L4), UNWRAP_ITEM_S(L5), UNWRAP_ITEM_S(L6), UNWRAP_ITEM_S(L7), UNWRAP_ITEM_S(L8), UNWRAP_ITEM_S(L9) }
#define LIST_S_8(L1, L2, L3, L4, L5, L6, L7, L8) { UNWRAP_ITEM_S(L1), UNWRAP_ITEM_S(L2), UNWRAP_ITEM_S(L3), UNWRAP_ITEM_S(L4), UNWRAP_ITEM_S(L5), UNWRAP_ITEM_S(L6), UNWRAP_ITEM_S(L7), UNWRAP_ITEM_S(L8) }
#define LIST_S_7(L1, L2, L3, L4, L5, L6, L7) { UNWRAP_ITEM_S(L1), UNWRAP_ITEM_S(L2), UNWRAP_ITEM_S(L3), UNWRAP_ITEM_S(L4), UNWRAP_ITEM_S(L5), UNWRAP_ITEM_S(L6), UNWRAP_ITEM_S(L7) }
#define LIST_S_6(L1, L2, L3, L4, L5, L6) { UNWRAP_ITEM_S(L1), UNWRAP_ITEM_S(L2), UNWRAP_ITEM_S(L3), UNWRAP_ITEM_S(L4), UNWRAP_ITEM_S(L5), UNWRAP_ITEM_S(L6) }
#define LIST_S_5(L1, L2, L3, L4, L5) { UNWRAP_ITEM_S(L1), UNWRAP_ITEM_S(L2), UNWRAP_ITEM_S(L3), UNWRAP_ITEM_S(L4), UNWRAP_ITEM_S(L5) }
#define LIST_S_4(L1, L2, L3, L4) { UNWRAP_ITEM_S(L1), UNWRAP_ITEM_S(L2), UNWRAP_ITEM_S(L3), UNWRAP_ITEM_S(L4) }
#define LIST_S_3(L1, L2, L3) { UNWRAP_ITEM_S(L1), UNWRAP_ITEM_S(L2), UNWRAP_ITEM_S(L3) }
#define LIST_S_2(L1, L2) { UNWRAP_ITEM_S(L1), UNWRAP_ITEM_S(L2) }
#define LIST_S_1(L1) { UNWRAP_ITEM_S(L1) }
#define LIST_S_(N) LIST_S_##N
#define LIST_S_EVAL(N) LIST_S_(N)
#define UNWRAP_S_2(...) EXPAND( LIST_S_EVAL(EXPAND( PP_NARG(__VA_ARGS__) ))(__VA_ARGS__) )
//#define UNWRAP_S_2(...) GET_MACRO(__VA_ARGS__, LIST_S_16, LIST_S_15, LIST_S_14, LIST_S_13, LIST_S_12, LIST_S_11, LIST_S_10, LIST_S_9, LIST_S_8, LIST_S_7, LIST_S_6, LIST_S_5, LIST_S_4, LIST_S_3, LIST_S_2, LIST_S_1)(__VA_ARGS__)
#define InternalsS(L) I_(UNWRAP_S_2 L)

#define ITEM_Sub(index, model) { index, model }
#define UNWRAP_ITEM_Sub(...) ITEM_Sub __VA_ARGS__
#define LIST_Sub_16(L1, L2, L3, L4, L5, L6, L7, L8, L9, L10, L11, L12, L13, L14, L15, L16) { UNWRAP_ITEM_Sub(L1), UNWRAP_ITEM_Sub(L2), UNWRAP_ITEM_Sub(L3), UNWRAP_ITEM_Sub(L4), UNWRAP_ITEM_Sub(L5), UNWRAP_ITEM_Sub(L6), UNWRAP_ITEM_Sub(L7), UNWRAP_ITEM_Sub(L8), UNWRAP_ITEM_Sub(L9), UNWRAP_ITEM_Sub(L10), UNWRAP_ITEM_Sub(L11), UNWRAP_ITEM_Sub(L12), UNWRAP_ITEM_Sub(L13), UNWRAP_ITEM_Sub(L14), UNWRAP_ITEM_Sub(L15), UNWRAP_ITEM_Sub(L16) }
#define LIST_Sub_15(L1, L2, L3, L4, L5, L6, L7, L8, L9, L10, L11, L12, L13, L14, L15) { UNWRAP_ITEM_Sub(L1), UNWRAP_ITEM_Sub(L2), UNWRAP_ITEM_Sub(L3), UNWRAP_ITEM_Sub(L4), UNWRAP_ITEM_Sub(L5), UNWRAP_ITEM_Sub(L6), UNWRAP_ITEM_Sub(L7), UNWRAP_ITEM_Sub(L8), UNWRAP_ITEM_Sub(L9), UNWRAP_ITEM_Sub(L10), UNWRAP_ITEM_Sub(L11), UNWRAP_ITEM_Sub(L12), UNWRAP_ITEM_Sub(L13), UNWRAP_ITEM_Sub(L14), UNWRAP_ITEM_Sub(L15) }
#define LIST_Sub_14(L1, L2, L3, L4, L5, L6, L7, L8, L9, L10, L11, L12, L13, L14) { UNWRAP_ITEM_Sub(L1), UNWRAP_ITEM_Sub(L2), UNWRAP_ITEM_Sub(L3), UNWRAP_ITEM_Sub(L4), UNWRAP_ITEM_Sub(L5), UNWRAP_ITEM_Sub(L6), UNWRAP_ITEM_Sub(L7), UNWRAP_ITEM_Sub(L8), UNWRAP_ITEM_Sub(L9), UNWRAP_ITEM_Sub(L10), UNWRAP_ITEM_Sub(L11), UNWRAP_ITEM_Sub(L12), UNWRAP_ITEM_Sub(L13), UNWRAP_ITEM_Sub(L14) }
#define LIST_Sub_13(L1, L2, L3, L4, L5, L6, L7, L8, L9, L10, L11, L12, L13) { UNWRAP_ITEM_Sub(L1), UNWRAP_ITEM_Sub(L2), UNWRAP_ITEM_Sub(L3), UNWRAP_ITEM_Sub(L4), UNWRAP_ITEM_Sub(L5), UNWRAP_ITEM_Sub(L6), UNWRAP_ITEM_Sub(L7), UNWRAP_ITEM_Sub(L8), UNWRAP_ITEM_Sub(L9), UNWRAP_ITEM_Sub(L10), UNWRAP_ITEM_Sub(L11), UNWRAP_ITEM_Sub(L12), UNWRAP_ITEM_Sub(L13) }
#define LIST_Sub_12(L1, L2, L3, L4, L5, L6, L7, L8, L9, L10, L11, L12) { UNWRAP_ITEM_Sub(L1), UNWRAP_ITEM_Sub(L2), UNWRAP_ITEM_Sub(L3), UNWRAP_ITEM_Sub(L4), UNWRAP_ITEM_Sub(L5), UNWRAP_ITEM_Sub(L6), UNWRAP_ITEM_Sub(L7), UNWRAP_ITEM_Sub(L8), UNWRAP_ITEM_Sub(L9), UNWRAP_ITEM_Sub(L10), UNWRAP_ITEM_Sub(L11), UNWRAP_ITEM_Sub(L12) }
#define LIST_Sub_11(L1, L2, L3, L4, L5, L6, L7, L8, L9, L10, L11) { UNWRAP_ITEM_Sub(L1), UNWRAP_ITEM_Sub(L2), UNWRAP_ITEM_Sub(L3), UNWRAP_ITEM_Sub(L4), UNWRAP_ITEM_Sub(L5), UNWRAP_ITEM_Sub(L6), UNWRAP_ITEM_Sub(L7), UNWRAP_ITEM_Sub(L8), UNWRAP_ITEM_Sub(L9), UNWRAP_ITEM_Sub(L10), UNWRAP_ITEM_Sub(L11) }
#define LIST_Sub_10(L1, L2, L3, L4, L5, L6, L7, L8, L9, L10) { UNWRAP_ITEM_Sub(L1), UNWRAP_ITEM_Sub(L2), UNWRAP_ITEM_Sub(L3), UNWRAP_ITEM_Sub(L4), UNWRAP_ITEM_Sub(L5), UNWRAP_ITEM_Sub(L6), UNWRAP_ITEM_Sub(L7), UNWRAP_ITEM_Sub(L8), UNWRAP_ITEM_Sub(L9), UNWRAP_ITEM_Sub(L10) }
#define LIST_Sub_9(L1, L2, L3, L4, L5, L6, L7, L8, L9) { UNWRAP_ITEM_Sub(L1), UNWRAP_ITEM_Sub(L2), UNWRAP_ITEM_Sub(L3), UNWRAP_ITEM_Sub(L4), UNWRAP_ITEM_Sub(L5), UNWRAP_ITEM_Sub(L6), UNWRAP_ITEM_Sub(L7), UNWRAP_ITEM_Sub(L8), UNWRAP_ITEM_Sub(L9) }
#define LIST_Sub_8(L1, L2, L3, L4, L5, L6, L7, L8) { UNWRAP_ITEM_Sub(L1), UNWRAP_ITEM_Sub(L2), UNWRAP_ITEM_Sub(L3), UNWRAP_ITEM_Sub(L4), UNWRAP_ITEM_Sub(L5), UNWRAP_ITEM_Sub(L6), UNWRAP_ITEM_Sub(L7), UNWRAP_ITEM_Sub(L8) }
#define LIST_Sub_7(L1, L2, L3, L4, L5, L6, L7) { UNWRAP_ITEM_Sub(L1), UNWRAP_ITEM_Sub(L2), UNWRAP_ITEM_Sub(L3), UNWRAP_ITEM_Sub(L4), UNWRAP_ITEM_Sub(L5), UNWRAP_ITEM_Sub(L6), UNWRAP_ITEM_Sub(L7) }
#define LIST_Sub_6(L1, L2, L3, L4, L5, L6) { UNWRAP_ITEM_Sub(L1), UNWRAP_ITEM_Sub(L2), UNWRAP_ITEM_Sub(L3), UNWRAP_ITEM_Sub(L4), UNWRAP_ITEM_Sub(L5), UNWRAP_ITEM_Sub(L6) }
#define LIST_Sub_5(L1, L2, L3, L4, L5) { UNWRAP_ITEM_Sub(L1), UNWRAP_ITEM_Sub(L2), UNWRAP_ITEM_Sub(L3), UNWRAP_ITEM_Sub(L4), UNWRAP_ITEM_Sub(L5) }
#define LIST_Sub_4(L1, L2, L3, L4) { UNWRAP_ITEM_Sub(L1), UNWRAP_ITEM_Sub(L2), UNWRAP_ITEM_Sub(L3), UNWRAP_ITEM_Sub(L4) }
#define LIST_Sub_3(L1, L2, L3) { UNWRAP_ITEM_Sub(L1), UNWRAP_ITEM_Sub(L2), UNWRAP_ITEM_Sub(L3) }
#define LIST_Sub_2(L1, L2) { UNWRAP_ITEM_Sub(L1), UNWRAP_ITEM_Sub(L2) }
#define LIST_Sub_1(L1) { UNWRAP_ITEM_Sub(L1) }
#define LIST_Sub_(N) LIST_Sub_##N
#define LIST_Sub_EVAL(N) LIST_Sub_(N)
#define UNWRAP_Sub_2(...) EXPAND( LIST_Sub_EVAL(EXPAND( PP_NARG(__VA_ARGS__) ))(__VA_ARGS__) )
//#define UNWRAP_Sub_2(...) GET_MACRO(__VA_ARGS__, LIST_Sub_16, LIST_Sub_15, LIST_Sub_14, LIST_Sub_13, LIST_Sub_12, LIST_Sub_11, LIST_Sub_10, LIST_Sub_9, LIST_Sub_8, LIST_Sub_7, LIST_Sub_6, LIST_Sub_5, LIST_Sub_4, LIST_Sub_3, LIST_Sub_2, LIST_Sub_1)(__VA_ARGS__)
#define Submodels(L) SM_(UNWRAP_Sub_2 L)


    }
}

#endif
