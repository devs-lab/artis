/**
 * @file artis/kernel/AbstractModel.hpp
 * @author See the AUTHORS file
 */

/*
 * Copyright (C) 2012-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ARTIS_KERNEL_ABSTRACT_MODEL_HPP
#define __ARTIS_KERNEL_ABSTRACT_MODEL_HPP

#include <artis/kernel/Node.hpp>
#include <artis/context/State.hpp>
#include <artis/utils/DoubleTime.hpp>
#include <artis/utils/Trace.hpp>

#include <boost/core/demangle.hpp>

#include <typeinfo>

#if WIN32
#include <iso646.h>
#endif

namespace artis {
    namespace kernel {

        template<typename Time, typename Parameters>
        class AbstractModel : public Node<Time> {
            typedef AbstractModel<Time, Parameters> type;

        public:
            AbstractModel(const AbstractModel<Time, Parameters>* parent = 0)
                    :
                    parent(parent), last_time(-1) { }

            virtual ~AbstractModel() { }

            virtual void after(typename Time::type t) = 0;

            virtual const Node<Time>* atomic(unsigned int index) const = 0;

            virtual void before(typename Time::type t) = 0;

            virtual bool check(typename Time::type /* t */) const { return true; }

            virtual void compute(typename Time::type t, bool update) = 0;

            virtual void init(typename Time::type t, const Parameters& parameters) = 0;

            virtual bool is_atomic() const = 0;

            virtual bool is_computed(typename Time::type t) const { return last_time == t; }

            virtual bool is_stable(typename Time::type t) const = 0;

            virtual bool is_computed(typename Time::type t,
                    unsigned int index) const = 0;

            virtual bool is_updated() const = 0;

            virtual void operator()(typename Time::type t)
            {
                before(t);
                if (check(t) and (last_time != t or
                        (last_time == t and is_updated()))) {
#ifdef WITH_TRACE
                    utils::Trace<utils::DoubleTime>::trace()
                            << utils::TraceElement<utils::DoubleTime>(
                                    true,
                                    AbstractModel<Time, Parameters>::path(this),
                                    t, utils::COMPUTE);
                    utils::Trace<utils::DoubleTime>::trace().flush();
#endif
                    compute(t, last_time == t);
                    last_time = t;
                    stable();
                }
                after(t);
            }

            virtual std::string path(
                    const AbstractModel<Time, Parameters>* child) const
            {
                int index = -1;
                std::string p = type::parent ? type::parent->path_index(child, index) :
                                "";

                if (index >= 0) {
                    return p +
                            "/[" + std::to_string(index) + "]" +
                            boost::core::demangle(typeid(*child).name()); // .erase(0,6);
                } else {
                    return (p.empty() ? "" : p + "/") +
                            boost::core::demangle(typeid(*child).name()); // .erase(0,6);
                }
            }

            virtual std::string path_index(
                    const AbstractModel<Time, Parameters>* child, int& index) const
            {
                (void) child;
                int i = -1;
                std::string p = type::parent ? type::parent->path_index(this, index) :
                                "";

                index = -1;
                if (i >= 0) {
                    return p +
                            "/[" + std::to_string(i) + "]" +
                            boost::core::demangle(typeid(*this).name());
                } else {
                    return (p.empty() ? "" : p + "/") +
                            boost::core::demangle(typeid(*this).name());
                }
            }

            virtual void restore(const context::State<Time>& state) = 0;

            virtual void save(context::State<Time>& state) const = 0;

            virtual void trace_element(typename Time::type t, utils::TraceType type,
                    std::string comment = "") const = 0;

            virtual void trace_internals(typename Time::type t,
                    utils::TraceType type) const = 0;

            virtual void trace_externals(typename Time::type t,
                    utils::TraceType type) const = 0;

            virtual void trace_model(typename Time::type t,
                    utils::TraceType type) const = 0;

            void set_parent(const AbstractModel<Time, Parameters>* p) { parent = p; }

            virtual void stable() = 0;

        protected:
            const AbstractModel<Time, Parameters>* parent;
            typename Time::type last_time;
        };

        template<typename Time, typename Parameters>
        using AbstractModels = std::vector<AbstractModel<Time, Parameters>*>;

    }
}

#endif
