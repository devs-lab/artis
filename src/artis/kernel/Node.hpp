/**
 * @file artis/kernel/Node.hpp
 * @author See the AUTHORS file
 */

/*
 * Copyright (C) 2012-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ARTIS_KERNEL_NODE_HPP
#define __ARTIS_KERNEL_NODE_HPP

#include <artis/kernel/Any.hpp>

#include <cassert>

namespace artis {
    namespace kernel {

        template<typename U>
        class Node {
        public:
            Node() { }

            virtual ~Node() { }

            virtual const Any& get(typename U::type t, unsigned int index) const = 0;

            virtual std::string get(const ValueTypeID& value_type, typename U::type t,
                    unsigned int index) const = 0;

            virtual const Node<U>* get_submodel(unsigned int /* index */) const
            {
                assert(false);
                return 0;
            }

            virtual const Node<U>* get_submodel(unsigned int /*index */,
                    unsigned int /* rank */) const
            {
                assert(false);
                return 0;
            }
        };

    }
}

#endif
