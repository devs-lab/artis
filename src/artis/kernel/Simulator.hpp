/**
 * @file artis/kernel/Simulator.hpp
 * @author See the AUTHORS file
 */

/*
 * Copyright (C) 2012-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_KERNEL_SIMULATOR_HPP
#define ARTIS_KERNEL_SIMULATOR_HPP

#include <artis/kernel/AbstractCoupledModel.hpp>
#include <artis/context/Context.hpp>
#include <artis/observer/Observer.hpp>
#include <artis/observer/View.hpp>
#include <artis/utils/DoubleTime.hpp>

namespace artis {
    namespace kernel {

        template<typename T, typename U, typename V, typename W>
        class Simulator {
        public:
            Simulator(artis::kernel::AbstractCoupledModel<T, U, V, W>* model,
                    W& parameters)
                    :_model(model), _observer(model) { _model->build(parameters); }

            Simulator(artis::kernel::AbstractCoupledModel<T, U, V, W>* model,
                    W& parameters, const std::string& json)
                    :_model(model), _observer(model) { _model->build(parameters, json); }

            virtual ~Simulator() { delete _model; }

            void attachView(const std::string& name, observer::View<U, V>* view) { _observer.attachView(name, view); }

            void init(double time, const V& parameters)
            {
                _model->init(time, parameters);
                _observer.init();

#ifdef WITH_TRACE
                _model->trace_model(true,
                        utils::DoubleTime::null,
                        utils::INIT);
#endif
            }

            const observer::Observer<U, V>& observer() const { return _observer; }

            void run(const context::Context<U>& context)
            {
                if (context.valid()) {
                    _model->restore(context.state());
                }
                for (double t = context.begin(); t <= context.end(); t++) {
                    (*_model)(t);
                    _observer.observe(t);
                }
            }

            void save(context::Context<U>& context) const
            {
                _model->save(context.state());
                context.saved();
            }

        private:
            artis::kernel::AbstractCoupledModel<T, U, V, W>* _model;
            observer::Observer<U, V> _observer;
        };

    }
}

#endif
