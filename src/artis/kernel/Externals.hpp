/**
 * @file artis/kernel/Externals.hpp
 * @author See the AUTHORS file
 */

/*
 * Copyright (C) 2012-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ARTIS_KERNEL_EXTERNALS_HPP
#define __ARTIS_KERNEL_EXTERNALS_HPP

#include <artis/kernel/Any.hpp>
#include <artis/kernel/Macro.hpp>

#include <vector>

#if WIN32
#include <iso646.h>
#endif
namespace artis {
    namespace kernel {

        template<typename T, typename U, typename V>
        class Externals {
            template<typename W>
            struct element {
                unsigned int index;
                const std::string name;
                W T::* var;

                element(unsigned int index, const std::string& name, W T::* var)
                        :
                        index(index), name(name), var(var) { }
            };

        public:
            Externals()
                    :updated(false) { }

            virtual ~Externals() { }

            const Any& get(unsigned int index) const { return externals.at(index).second; }

            const std::string& name(unsigned int index) const { return external_names.at(index); }

            bool check(typename U::type t) const
            {
                bool OK = true;
                typename std::vector<
                        std::pair<double,
                                Any> >::const_iterator it = externals.begin();

                while (it != externals.end() and OK) {
                    OK = it->first == t;
                    ++it;
                }
                return OK;
            }

            template<typename W>
            void E_(std::initializer_list<element<W> > list)
            {
                for (typename std::initializer_list<element<W> >::iterator it =
                        list.begin(); it != list.end(); ++it) {
                    if (externals.size() <= it->index) {
                        externals.resize(it->index + 1, std::make_pair(-1, Any()));
                        external_names.resize(it->index + 1, std::string());
                    }
                    externals[it->index] = std::make_pair(-1, it->var);
                    external_names[it->index] = it->name;

#ifdef WITH_TRACE
                    utils::Trace<utils::DoubleTime>::trace()
                            << utils::TraceElement<utils::DoubleTime>(
                                    true,
                                    dynamic_cast<T*>(this)->path(dynamic_cast<T*>(this)),
                                    utils::DoubleTime::null,
                                    utils::EXTERNAL_DECL)
                            << utils::KernelInfo(it->name, false);
                    utils::Trace<utils::DoubleTime>::trace().flush();
#endif
                }
            }

            template<typename W>
            void external_(unsigned int index, const std::string& name, W T::* var)
            {
                if (externals.size() <= index) {
                    externals.resize(index + 1, std::make_pair(-1, Any()));
                    external_names.resize(index + 1, std::string());
                }
                externals[index] = std::make_pair(-1, var);
                external_names[index] = name;

#ifdef WITH_TRACE
                utils::Trace<utils::DoubleTime>::trace()
                        << utils::TraceElement<utils::DoubleTime>(
                                true,
                                dynamic_cast<T*>(this)->path(dynamic_cast<T*>(this)),
                                utils::DoubleTime::null,
                                utils::EXTERNAL_DECL)
                        << utils::KernelInfo(name, false);
                utils::Trace<utils::DoubleTime>::trace().flush();
#endif
            }

            bool is_ready(typename U::type t, unsigned int index) const { return externals.at(index).first == t; }

            template<typename W>
            void put(typename U::type t, unsigned int index, W value)
            {
                if (externals.at(index).first != t) {
                    Any v = externals.at(index).second;

                    v.put<T, W>(static_cast < T* >(this), value);
                    externals.at(index).first = t;
                    updated = true;
                } else {
                    Any v = externals.at(index).second;

                    if (static_cast < const T* >(this)->*(v.get<T, W>()) != value) {
                        v.put<T, W>(static_cast < T* >(this), value);
                        updated = true;
                    }
                }

#ifdef WITH_TRACE
                utils::Trace<utils::DoubleTime>::trace()
                        << utils::TraceElement<utils::DoubleTime>(
                                true,
                                dynamic_cast<T*>(this)->path(dynamic_cast<T*>(this)),
                                t, utils::PUT)
                        << utils::KernelInfo(external_names[index], true,
                                get(index).to_string(dynamic_cast < const T* >(this)));
                utils::Trace<utils::DoubleTime>::trace().flush();
#endif
            }

            virtual void restore(AbstractModel<U, V>* model,
                    const context::State<U>& state)
            {
                unsigned int index = 0;

                for (typename std::vector<std::pair<typename U::type,
                        Any> >::iterator it = externals.begin();
                     it != externals.end(); ++it) {
                    Any& value = it->second;

                    if (not value.is_null()) {
                        value.restore<T>(static_cast < T* >(model),
                                state.get_external(index));
                    }
                    ++index;
                }
            }

            virtual void save(const AbstractModel<U, V>* model,
                    context::State<U>& state) const
            {
                unsigned int index = 0;

                for (typename std::vector<std::pair<typename U::type,
                        Any> >::const_iterator it = externals.begin();
                     it != externals.end(); ++it) {
                    const Any& value = it->second;

                    if (not value.is_null()) {
                        state.add_external(index,
                                value.save<T>(
                                        static_cast < const T* >(model)));
                    }
                    ++index;
                }
            }

            unsigned int size() const { return externals.size(); }

        protected:
            bool updated;
            std::vector<std::pair<typename U::type, Any> > externals;
            std::vector<std::string> external_names;
        };

#define External(index, var)                                    \
        external_(index, std::string(ESCAPEQUOTE(index)), var)

#define Externals(W, L) E_< W >(UNWRAP2 L)

    }
} // namespace artis kernel

#endif
