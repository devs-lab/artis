/**
 * @file artis/kernel/States.hpp
 * @author See the AUTHORS file
 */

/*
 * Copyright (C) 2012-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ARTIS_KERNEL_STATES_HPP
#define __ARTIS_KERNEL_STATES_HPP

#include <artis/context/State.hpp>
#include <artis/kernel/Any.hpp>

#include <vector>

namespace artis {
    namespace kernel {

        template<typename T, typename U, typename V>
        class States {
            template<typename W>
            struct element {
                unsigned int index;
                const std::string name;
                W T::* var;

                element(unsigned int index, const std::string& name, W T::* var)
                        :
                        index(index), name(name), var(var) { }
            };

        public:
            States() { }

            virtual ~States() { }

            const Any& get(unsigned int index) const { return states.at(index); }

            template<typename W>
            void S_(std::initializer_list<element<W> > list)
            {
                for (typename std::initializer_list<element<W> >::iterator it =
                        list.begin(); it != list.end(); ++it) {
                    if (states.size() <= it->index) {
                        states.resize(it->index + 1, Any());
                        state_names.resize(it->index + 1, std::string());
                    }
                    states[it->index] = it->var;
                    state_names[it->index] = it->name;
                }
            }

            virtual void restore(AbstractModel<U, V>* model,
                    const context::State<U>& state)
            {
                unsigned int index = 0;

                for (typename std::vector<Any>::iterator it = states.begin();
                     it != states.end(); ++it) {
                    if (not it->is_null()) {
                        it->restore<T>(static_cast < T* >(model),
                                state.get_state(index));
                    }
                    ++index;
                }
            }

            virtual void save(const AbstractModel<U, V>* model,
                    context::State<U>& state) const
            {
                unsigned int index = 0;

                for (typename std::vector<Any>::const_iterator it =
                        states.begin(); it != states.end(); ++it) {
                    if (not it->is_null()) {
                        state.add_state(index,
                                it->save<T>(
                                        static_cast < const T* >(model)));
                    }
                    ++index;
                }
            }

            template<typename W>
            void state_(unsigned int index, const std::string& name, W T::* var)
            {
                if (states.size() <= index) {
                    states.resize(index + 1, Any());
                    state_names.resize(index + 1, std::string());
                }
                states[index] = Any(var);
                state_names[index] = name;
            }

        private:
            std::vector<Any> states;
            std::vector<std::string> state_names;
        };

#define State(index, var)                                    \
        state_(index, std::string(ESCAPEQUOTE(index)), var)

#define States(W, L) S_< W >(UNWRAP2 L)

    }
}

#endif
