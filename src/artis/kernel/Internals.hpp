/**
 * @file artis/kernel/Internals.hpp
 * @author See the AUTHORS file
 */

/*
 * Copyright (C) 2012-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __ARTIS_KERNEL_INTERNALS_HPP
#define __ARTIS_KERNEL_INTERNALS_HPP

#include <artis/context/State.hpp>
#include <artis/kernel/Any.hpp>
#include <artis/kernel/Macro.hpp>

#include <vector>

namespace artis {
    namespace kernel {

        template<typename T, typename U, typename V>
        class Internals {
            template<typename W>
            struct element {
                unsigned int index;
                const std::string name;
                W T::* var;

                element(unsigned int index, const std::string& name, W T::* var)
                        :
                        index(index), name(name), var(var) { }
            };

        public:
            Internals() { }

            virtual ~Internals() { }

            const Any& get(unsigned int index) const { return internals.at(index); }

            template<typename W>
            void I_(std::initializer_list<element<W> > list)
            {
                for (typename std::initializer_list<element<W> >::iterator it =
                        list.begin(); it != list.end(); ++it) {
                    if (internals.size() <= it->index) {
                        internals.resize(it->index + 1);
                        internal_names.resize(it->index + 1, std::string());
                    }
                    internals[it->index] = it->var;
                    internal_names[it->index] = it->name;

#ifdef WITH_TRACE
                    utils::Trace<utils::DoubleTime>::trace()
                            << utils::TraceElement<utils::DoubleTime>(
                                    true,
                                    dynamic_cast<T*>(this)->path(dynamic_cast<T*>(this)),
                                    utils::DoubleTime::null,
                                    utils::INTERNAL_DECL)
                            << utils::KernelInfo(it->name, true);
                    utils::Trace<utils::DoubleTime>::trace().flush();
#endif
                }

            }

            template<typename W>
            void internal_(unsigned int index, const std::string& name, W T::* var)
            {
                if (internals.size() <= index) {
                    internals.resize(index + 1, Any());
                    internal_names.resize(index + 1, std::string());
                }
                internals[index] = Any(var);
                internal_names[index] = name;

#ifdef WITH_TRACE
                utils::Trace<utils::DoubleTime>::trace()
                        << utils::TraceElement<utils::DoubleTime>(
                                true,
                                dynamic_cast<T*>(this)->path(dynamic_cast<T*>(this)),
                                utils::DoubleTime::null,
                                utils::INTERNAL_DECL)
                        << utils::KernelInfo(name, true);
                utils::Trace<utils::DoubleTime>::trace().flush();
#endif
            }

            const std::string& name(unsigned int index) const { return internal_names.at(index); }

            virtual void restore(AbstractModel<U, V>* model,
                    const context::State<U>& state)
            {
                unsigned int index = 0;

                for (typename std::vector<Any>::iterator it = internals.begin();
                     it != internals.end(); ++it) {
                    if (not it->is_null()) {
                        it->restore<T>(static_cast < T* >(model),
                                state.get_internal(index));
                    }
                    ++index;
                }
            }

            virtual void save(const AbstractModel<U, V>* model,
                    context::State<U>& state) const
            {
                unsigned int index = 0;

                for (typename std::vector<Any>::const_iterator it =
                        internals.begin(); it != internals.end(); ++it) {
                    if (not it->is_null()) {
                        state.add_internal(index,
                                it->save<T>(
                                        static_cast < const T* >(model)));
                    }
                    ++index;
                }
            }

            unsigned int size() const { return internals.size(); }

        private:
            std::vector<std::string> internal_names;
            std::vector<Any> internals;
        };

#define Internal(index, var)                                    \
    internal_(index, std::string(ESCAPEQUOTE(index)), var)

#define Internals(W, L) I_< W >(UNWRAP2 L)

    }
} // namespace artis kernel

#endif
