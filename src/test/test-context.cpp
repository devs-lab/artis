/**
 * @file test/test-context.cpp
 * @author See the AUTHORS file
 */

/*
 * Copyright (C) 2012-2019 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <test/models.hpp>

#include <artis/kernel/Simulator.hpp>
#include <artis/utils/DateTime.hpp>

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include <fstream>

typedef artis::kernel::Simulator<RootModel, artis::utils::DoubleTime, ModelParameters, GlobalParameters> Simulator;
typedef artis::context::Context<artis::utils::DoubleTime> Context;

int main()
{
    GlobalParameters globalParameters;
    ModelParameters modelParameters;

    Context context(artis::utils::DateTime::toJulianDayNumber("2016-1-1"),
            artis::utils::DateTime::toJulianDayNumber("2016-1-5"));

    Trace::trace().clear();

    {
        Simulator simulator(new RootModel, globalParameters);

        simulator.init(artis::utils::DateTime::toJulianDayNumber("2016-1-1"), modelParameters);
        simulator.run(context);
        simulator.save(context);

        std::ofstream os("state");
        boost::archive::binary_oarchive oa(os);

        oa << context;
    }

    std::cout << ::Trace::trace().elements().filter_variable("BX").to_string(artis::utils::DATE_FORMAT_YMD)
              << std::endl;

    std::cout << "==== PAUSE ====" << std::endl << std::endl;

    Trace::trace().clear();

    {
        Context new_context(context);
        Simulator simulator(new RootModel, globalParameters);

        new_context.begin(artis::utils::DateTime::toJulianDayNumber("2016-1-6"));
        new_context.end(artis::utils::DateTime::toJulianDayNumber("2016-1-10"));
        simulator.run(new_context);
    }

    std::cout << ::Trace::trace().elements().filter_type(artis::utils::COMPUTE).to_string() << std::endl;

    std::cout << "==== PAUSE ====" << std::endl << std::endl;;

    Trace::trace().clear();

    {
        Context new_context;
        Simulator simulator(new RootModel, globalParameters);
        std::ifstream is("state");
        boost::archive::binary_iarchive ia(is);

        ia >> new_context;

        new_context.begin(artis::utils::DateTime::toJulianDayNumber("2016-1-6"));
        new_context.end(artis::utils::DateTime::toJulianDayNumber("2016-1-10"));
        simulator.run(new_context);
    }

    std::cout << ::Trace::trace().elements().filter_type(artis::utils::COMPUTE).to_string() << std::endl;

    return 0;
}
